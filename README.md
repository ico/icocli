# ICOcli


This project is a command-line client of the ICO tool suite.



## Developement
This project relies on several libraries. You must add them to your classpath in to order to compile the project:
- [de.ovgu.featureide.lib.fm-v3.9.0.jar](https://github.com/FeatureIDE/FeatureIDE/releases/download/v3.9.0/de.ovgu.featureide.lib.fm-v3.9.0.jar)
- [commons-cli-1.5.0.jar](https://dlcdn.apache.org//commons/cli/binaries/commons-cli-1.5.0-bin.zip) (make sur to select the correct jar within the zip file)
- [antler-3.4.jar](https://github.com/FeatureIDE/FeatureIDE/raw/9666caac13b4494bcbe773aac5c09569c24a227f/plugins/de.ovgu.featureide.fm.core/lib/antlr-3.4.jar)
- [uvl-parser.jar](https://github.com/FeatureIDE/FeatureIDE/raw/9666caac13b4494bcbe773aac5c09569c24a227f/plugins/de.ovgu.featureide.fm.core/lib/uvl-parser.jar)
- [org.sat4j.core.jar](https://github.com/FeatureIDE/FeatureIDE/raw/9666caac13b4494bcbe773aac5c09569c24a227f/plugins/de.ovgu.featureide.fm.core/lib/org.sat4j.core.jar)
- [icolib.jar](https://gitlab.inria.fr/ico/icolib)

Complete installation on Eclipse:
- clone the project with ```git@gitlab.inria.fr:ico/icocli.git```
- Open the project with Eclipse: File -> "Open Project from file system" -> with the "directory" button, browse to the location where you cloned the project, and select the icocli folder. Select "open" to select this folder. Make sure that the "icocli" project is selected in the project import window and select "finish".
- Add the dependancies to your classpath: Right click on the icolib project in Eclipse, "Build Path" -> "Configure Build Path". In the project properties window, select "classpath" and then "Add external JARs". Browse to the location where you downloaded the dependancies, select the six of them and select "open". In the project properties window, select "Apply and close".

The project is now ready to compile. To test the project, right-click on icocli in Eclipse -> "Run as" -> "Java Application" -> "Interpreter - fr.ico.cli" -> "ok"

To export as a runable jar:
- Right click on icocli in Eclipse -> "Export"
- Select "Runnable JAR file" -> "Next"
- Configure the export destination to where you want the jar file.
- Make sure that "Extract required libraries into generated JAR" is selected
- Select "Finish", and then "ok" if a warning appears about licences of libraries.
- The JAR can then be run with "java -jar \<file_name\>.jar"



## Usage
The ICOcli project can take instruction either as startup parameters, and in interactive mode.
### Startup parameters: 
| Option                 | Short option | Parameter       | Description                                                                                                           |
|------------------------|--------------|-----------------|-----------------------------------------------------------------------------------------------------------------------|
| --project              | -P           | Path            | The path to the project                                                                                               |
| --model                | -M           | Path            | The path to the model within the project (./model.xml by default)                                                     |
| --config               | -C           | Path            | The path to the configuration within the project (./configs/default.xml by default)                                   |
| --feature (by default) | -F           |                 | Feature-wise mode                                                                                                     |
| --interaction          | -I           |                 | Interaction-wise mode                                                                                                 |
| --include              | -i           | list of feature | Specifies the comma-separated list of required-in feature                                                             |
| --exclude              | -e           | list of feature | Specifies the comma-separated list of required-out features                                                           |
| --list                 | -L           |                 | List the suggestions of the corresponding mode                                                                        |
| --apply                | -a           | index           | Apply the suggestion at the given index in the corresponding mode                                                     |
| --apply-all            | -A           |                 | Apply all successive best suggestions of the corresponding mode                                                       |
| --save                 | -s           | Path (optional) | Override the configuration file with current changes, or save the configuration and current changes to the given path |
| --no-shell             | -n           |                 | Prevent the shell from opening                                                                                        |

Examples of startup parameters usage : 

```java -jar icocli.jar -P ./project -M model.xml -C configs/default.xml -e feat1,feat2 -i feat3 -F -A -S -N```

This command:
- Load the project ./project, the model  ./project/model.xml, the configuraiton ./project/configs/default.xml
- exclude the features feat1 and feat2, and inclues feat3
- set the mode as feature-wise
- Apply all the best suggestions of the feature-wise mode
- Override the configuration file with current changes
- Prevent the opening of the shell

### Interactive mode:
In interactive mode, ICOcli accepts the following parameters:
| Command     | Short command | Description                                        |
|-------------|---------------|----------------------------------------------------|
| suggestion  | sug           | List suggestions                                   |
| apply       |               | Apply one or all suggestions                       |
| performance | perf          | List the performances of the current configuration |
| load        |               | Load a project                                     |
| info        |               | Provide information about the current project      |
| include     | inc           | Manage the required-in features                    |
| exclude     | exc           | Manage the required-out features                   |
| save        |               | Save the configuration                             |
| oneline     | ol            | Mimics the behavior of the startup parameters      | 
| change      |               | Create a user-defined suggestion                   |
| exit        |               | Leave the program                                  | 

Additional information can be found by calling each command with the "-h" parameter.


Please note that it is possible to mix startup parameters and interactive mode. For instance:

```
java -jar ./icocli.jar -P ./project -M model.xml -C configs/default.xml
> exclude --add feat1,feat2
> include --add feat3
> apply -F -A
> save
> exit
```

- Load the project ./project, the model  ./project/model.xml, the configuraiton ./project/configs/default.xml
- exclude the features feat1 and feat2, and inclues feat3
- set the mode as feature-wise
- Apply all the best suggestions of the feature-wise mode
- Override the configuration file with current changes
- Exit the program

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
If you want to contribute to this project please create a merge request.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.


