package fr.ico.cli;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import de.ovgu.featureide.fm.core.init.FMCoreLibrary;
import de.ovgu.featureide.fm.core.init.LibraryManager;
import fr.ico.cli.command.MainArgs;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.utils.Logger;


public class Interpereter {

	public static void main(String[] args) throws IOException {

		// load -P /Users/edouardguegain/runtime-EclipseTestbed/sss -M model.xml -C configs/default.xml
		// load -P /home/mitrail/runtime-EclipseApplication/sss -M model.xml -C configs/default.xml
		
		boolean openCLI = true;
		LibraryManager.registerLibrary(FMCoreLibrary.getInstance());
		Interpereter interpereter = new Interpereter(new Scanner(System.in));

		if (args.length > 0) {
			MainArgs mainArgs = new MainArgs();
			openCLI = mainArgs.execute(args, interpereter);
		}
	
		if (openCLI) {
			interpereter.readCommand();
		}
		System.out.println("Leaving program");
	}

	private ConfigImprovement configImprovement;
	private Scanner console;
	
	public Interpereter(Scanner sc) {
		this.console = sc;
	}
	
	public int readCommand() throws IOException {
		System.out.print("> ");
		String command = console.nextLine();
		if (command.equals("exit")) return 0;
		try {
			parseCommand(command.split("\\s+"));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return readCommand();
	}
	
	public void parseCommand(String[] command) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Map<String, String> shortcuts = new HashMap<>();
		shortcuts.put("sug", "Suggestion");
		shortcuts.put("suggestion", "Suggestion");
		shortcuts.put("apply", "Apply");
		shortcuts.put("perf", "Performance");
		shortcuts.put("performance", "Performance");
		shortcuts.put("load", "Load");
		shortcuts.put("info", "Info");
		shortcuts.put("inc", "Include");
		shortcuts.put("include", "Include");
		shortcuts.put("exc", "Exclude");
		shortcuts.put("exclude", "Exclude");
		shortcuts.put("save", "Save");
		shortcuts.put("oneline", "MainArgs");
		shortcuts.put("ol", "MainArgs");
		shortcuts.put("change", "Change");
		
		
		if (!shortcuts.containsKey(command[0])) {
			Logger.log("Invalide command. Use one of:");
			shortcuts.entrySet().forEach(e -> Logger.log(e.getKey()));
		} else {
			Class<? extends Command> activityClass = Class.forName("fr.ico.cli.command." + shortcuts.get(command[0])).asSubclass(Command.class);
			Constructor<? extends Command> constructor = activityClass.getConstructor();
			Command commandObject = constructor.newInstance();
			commandObject.execute(command, this);
		}
		
	}
	
	public void info() throws NoConfigurationException {
		Logger.log("Is configuration valid: " + getConfigImprovement().isConfigValid());
		Logger.log("Can configuration be valid: " + getConfigImprovement().canConfigValid());
		Logger.log("whitelist : " + getConfigImprovement().getIncludeNames());
		Logger.log("blacklist : " + getConfigImprovement().getExcludeNames());
		Logger.log("Selected features: " + String.join(",", getConfigImprovement().getSelectedFeatureNames()));
		Logger.log("Available features: " + String.join(",", getConfigImprovement().getSelectableFeatureNames()));
	}

	public ConfigImprovement getConfigImprovement() throws NoConfigurationException {
		if (configImprovement == null) throw new NoConfigurationException();
		return configImprovement;
	}

	public void setConfigImprovement(ConfigImprovement configImprovement) {
		this.configImprovement = configImprovement;
	}
	
}
