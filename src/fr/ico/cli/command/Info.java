package fr.ico.cli.command;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;

public class Info extends Command {
	
	public Info() {
		super("info");
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws ParseException, NoConfigurationException {
		interpereter.info();
		return true;
	}

}
