package fr.ico.cli.command;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.Optimization;

public class Suggestion extends Command {
	
	public Suggestion() {
		super("suggestion");
		
		Option option_r = Option.builder("F").desc("List feature suggestions").longOpt("feature").build();
		Option option_S = Option.builder("I").desc("List interaction suggestions").longOpt("interaction").build();

		getOptions().addOption(option_r);
		getOptions().addOption(option_S);
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws NoConfigurationException {
		ConfigImprovement configImprovement = interpereter.getConfigImprovement();

		if (!commandLine.hasOption("I")) {
			list(configImprovement, configImprovement.getFeatureOptimization());
		}
		
		if (!commandLine.hasOption("F")) {
			list(configImprovement, configImprovement.getInteractionOptimization());
		}
		
		return true;
	}
	
	public static void list(ConfigImprovement configImprovement, Optimization optimization) {
		configImprovement.getSuggestions(optimization, true);
	}
	
	

}
