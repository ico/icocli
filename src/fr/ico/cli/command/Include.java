package fr.ico.cli.command;

import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;

public class Include extends Command {
	
	public Include() {
		super("include");
		
		Option option_r = Option.builder("a").desc("The comma-separated list of features to add to inclusions").hasArgs().valueSeparator(',').longOpt("add").build();
		Option option_S = Option.builder("r").desc("The comma-separated list of features to remove from inclusions").hasArgs().valueSeparator(',').longOpt("remove").build();
		
		getOptions().addOption(option_r);
		getOptions().addOption(option_S);
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws ParseException, NoConfigurationException {
		ConfigImprovement configImprovement = interpereter.getConfigImprovement();
		if (commandLine.hasOption("a")) {
			Arrays.asList(commandLine.getOptionValues("a")).forEach(configImprovement::addInclusion);
		} else if (commandLine.hasOption("r")) {
			Arrays.asList(commandLine.getOptionValues("r")).forEach(configImprovement::removeInclusion);
		}
		return true;

	}

}
