package fr.ico.cli.command;

import java.nio.file.Path;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.utils.Logger;

public class Save extends Command {
	
	public Save() {
		super("save");
		
		Option option_p = Option.builder("p").desc("Specify where to save the file").hasArg().longOpt("path").build();

		getOptions().addOption(option_p);
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws ParseException, NoConfigurationException {
		return save(interpereter.getConfigImprovement(), commandLine.hasOption("p") ? commandLine.getOptionValue("P") : null);
	}
	
	public static boolean save(ConfigImprovement configImprovement, String pathString) {
		boolean saved = false;
		
		if (pathString != null) {
			Logger.log("Saving to " + pathString.toString());
			Path path = Path.of(pathString);
			saved = configImprovement.save(path);
		} else {
			Logger.log("Overwriting file");
			saved = configImprovement.save();
		}
		
		if(saved == false) {
			Logger.err("Unspecified error while saving");
		} else {
			Logger.log("Successfully saved");
		}

		return saved;
	}

}
