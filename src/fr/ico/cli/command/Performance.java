package fr.ico.cli.command;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.Optimization;
import fr.ico.lib.optimization.utils.Logger;

public class Performance extends Command {
	
	public Performance() {
		super("performance");
		
		Option option_f = Option.builder("F").desc("Display only feature performances").longOpt("feature").build();
		Option option_i = Option.builder("I").desc("Display only interaction performances").longOpt("interaction").build();

		getOptions().addOption(option_f);
		getOptions().addOption(option_i);
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws ParseException, NoConfigurationException {
		Optimization featOptimization = interpereter.getConfigImprovement().getFeatureOptimization();
		Optimization interactionOptimization = interpereter.getConfigImprovement().getInteractionOptimization();
		if (!commandLine.hasOption("I") && !featOptimization.getPerformance().isEmpty()) {
			Logger.log("-- Feature performances --");
			interpereter.getConfigImprovement().getPerformanceItems(featOptimization).forEach(System.out::println);
		}
		
		if (!commandLine.hasOption("F") && !interactionOptimization.getPerformance().isEmpty()) {
			Logger.log("-- Interaction performances --");
			interpereter.getConfigImprovement().getPerformanceItems(interactionOptimization).forEach(System.out::println);
		} 

		return true;
	}

}
