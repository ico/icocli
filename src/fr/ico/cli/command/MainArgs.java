package fr.ico.cli.command;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.Optimization;
import fr.ico.lib.optimization.utils.Logger;

public class MainArgs extends Command {

	public MainArgs() {
		super("ICOcli");
		// project
		Option option_p = Option.builder("P").required().desc("Path to the project").hasArg().longOpt("project").build();
		Option option_c = Option.builder("C").desc("Path to the configuration within the project. Default: " + Load.DEFAULT_CONFIG).hasArg().longOpt("config").build();
		Option option_m = Option.builder("M").desc("Path to the model within the project. Default: " + Load.DEFAULT_MODEL).hasArg().longOpt("model").build();

		// mode 
		Option option_f = Option.builder("F").desc("Work in feature mode").longOpt("feature").build();
		Option option_i = Option.builder("I").desc("Work in interaction mode").longOpt("interaction").build();

		// constraints
		Option option_include = Option.builder("i").desc("The comma-separated list of features to include").hasArgs().valueSeparator(',').longOpt("include").build();
		Option option_exclude = Option.builder("e").desc("The comma separated list of features to exclude").hasArgs().valueSeparator(',').longOpt("exclude").build();

		// list
		Option option_l = Option.builder("L").desc("List suggestions").longOpt("list").build();
		
		// apply
		Option option_a = Option.builder("a").desc("Apply the suggestion of the specified index").hasArg().type(Number.class).longOpt("apply").build();
		Option option_A = Option.builder("A").desc("Apply all suggestions").longOpt("apply-all").build();
		
		// save
		Option option_s = Option.builder("S").desc("Save the file").hasArg().optionalArg(true).longOpt("save").build();
		
		// open shell
		Option option_ns = Option.builder("N").desc("Prevent the shell from opening").longOpt("no-shell").build();

		
		getOptions().addOption(option_p);
		getOptions().addOption(option_c);
		getOptions().addOption(option_m);

		getOptions().addOption(option_f);
		getOptions().addOption(option_i);

		getOptions().addOption(option_include);
		getOptions().addOption(option_exclude);

		getOptions().addOption(option_l);
		
		getOptions().addOption(option_a);
		getOptions().addOption(option_A);

		getOptions().addOption(option_s);
		getOptions().addOption(option_ns);
		
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws ParseException, NoConfigurationException {
		// load project
		String project = commandLine.getOptionValue("P");
		String config = Load.DEFAULT_CONFIG;
		String fm = Load.DEFAULT_MODEL;
		
		if (commandLine.hasOption("C")) {
			config = commandLine.getOptionValue("C");
		}
		if (commandLine.hasOption("M")) {
			fm = commandLine.getOptionValue("M");
		}
		
		try {
			Load.handling(project, config, fm, interpereter);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ConfigImprovement configImprovement = interpereter.getConfigImprovement();
		
		// feature/interaction mode
		boolean interactionMode = commandLine.hasOption("I");
		Logger.log("Working in " + (interactionMode ? "interaction" : "feature")+ " mode");
		Optimization optimization = interactionMode ? configImprovement.getInteractionOptimization() : configImprovement.getFeatureOptimization();
		
		// constraints
		if (commandLine.hasOption("e")) {
			Arrays.asList(commandLine.getOptionValues("e")).forEach(interpereter.getConfigImprovement()::addExclusion);
		}
		if (commandLine.hasOption("i")) {
			Arrays.asList(commandLine.getOptionValues("i")).forEach(interpereter.getConfigImprovement()::addInclusion);
		}
		
		// list suggestions
		if (commandLine.hasOption("L")) {
			Suggestion.list(interpereter.getConfigImprovement(), optimization);
		}
		
		// apply suggestions
		if (commandLine.hasOption("A")) {
			Apply.applyAll(interpereter.getConfigImprovement(), optimization);
		} else if (commandLine.hasOption("a")) {
			Number index = (Number) commandLine.getParsedOptionValue("a");
			Apply.apply(interpereter.getConfigImprovement(), optimization, index.intValue());
		}
		
		// save
		if (commandLine.hasOption("S")) {
			String argPath = commandLine.getOptionValue("S");
			Save.save(interpereter.getConfigImprovement(), argPath);
		} 
		
		return !commandLine.hasOption("N");
	}

}
