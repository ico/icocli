package fr.ico.cli.command;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.Optimization;
import fr.ico.lib.optimization.storage.Suggestion;
import fr.ico.lib.optimization.utils.Logger;

public class Apply extends Command {
	
	public Apply() {
		super("apply");
		
		Option option_f = Option.builder("F").desc("Apply the feature suggestion of the specified index or all feature suggestions").hasArg().optionalArg(true).type(Number.class).longOpt("feature").build();
		Option option_i = Option.builder("I").desc("Apply the interaction suggestion of the specified index or all interaction suggestions").hasArg().optionalArg(true).type(Number.class).longOpt("interaction").build();
		Option option_A = Option.builder("A").desc("Apply all suggestions").longOpt("apply-all").build();
		
		getOptions().addOption(option_f);
		getOptions().addOption(option_i);
		getOptions().addOption(option_A);
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws ParseException, NoConfigurationException {
		Optimization optimization = interpereter.getConfigImprovement().getFeatureOptimization();
		Number index = -1;
		
		if (commandLine.hasOption("F")) {
			//index = commandLine.getOptionValue("F");
			index = (Number) commandLine.getParsedOptionValue("F");
		} else if (commandLine.hasOption("I")) {
			optimization = interpereter.getConfigImprovement().getInteractionOptimization();
			index = (Number) commandLine.getParsedOptionValue("I");
		} 
		
		int applied = 0;
		if (commandLine.hasOption("A")) {
			applied = applyAll(interpereter.getConfigImprovement(), optimization);
		} else if (index.intValue() != -1){
			applied = apply(interpereter.getConfigImprovement(), optimization, index.intValue());
		} else {
			Logger.log("No suggestion specified");
		}
		Logger.log("Suggestion(s) applied: " +  applied);
		
		return true;
	}
	
	public static int applyAll(ConfigImprovement configImprovement, Optimization optimization) {
		return configImprovement.applyAllSuggestions(optimization).size();
	}
	
	public static int apply(ConfigImprovement configImprovement, Optimization optimization, int index) {
		List<Suggestion> suggestions = configImprovement.getSuggestions(optimization, false);
		int applied = 0;
		if (index < suggestions.size()) {
			Suggestion sugg = configImprovement.getSuggestions(optimization, false).get(index);
			configImprovement.applySuggestion(sugg);
			applied = 1;
		} else {
			Logger.log("Invalide arguement " + index + ": only " + suggestions.size() + " suggetsions.");
		}
		return applied;
	}
	
}
