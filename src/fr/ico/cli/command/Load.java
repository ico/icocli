package fr.ico.cli.command;

import java.io.IOException;
import java.nio.file.Path;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.utils.FileHandling;

public class Load extends Command {

	public static final String DEFAULT_CONFIG = "./config/default.xml";
	public static final String DEFAULT_MODEL = "./model.xml";
	
	public Load() {
		super("load");
		
		Option option_p = Option.builder("P").required().desc("Path to the project").hasArg().longOpt("project").build();
		Option option_c = Option.builder("C").desc("Path to the configuration within the project. Default: " + DEFAULT_CONFIG).hasArg().longOpt("config").build();
		Option option_m = Option.builder("M").desc("Path to the model within the project. Default: " + DEFAULT_MODEL).hasArg().longOpt("model").build();

		getOptions().addOption(option_p);
		getOptions().addOption(option_c);
		getOptions().addOption(option_m);
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter) throws ParseException, NoConfigurationException {
		String project;
		String config = DEFAULT_CONFIG;
		String fm = DEFAULT_MODEL;
		
		project = commandLine.getOptionValue("P");
		if (commandLine.hasOption("C")) {
			config = commandLine.getOptionValue("C");
		}
		if (commandLine.hasOption("M")) {
			fm = commandLine.getOptionValue("M");
		}
		
		try {
			handling(project, config, fm, interpereter);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean handling(String project, String config, String fm, Interpereter interpereter) throws IOException, NoConfigurationException {
		Path projectPath = Path.of(project);
		Path configPath = Path.of(project + "/" + config);
		Path fmPath = Path.of(project + "/" + fm);
		
		ConfigImprovement configImprovement = FileHandling.loadProjectFiles(projectPath, configPath);
		configImprovement.attachFeatureModel(fmPath);
		
		interpereter.setConfigImprovement(configImprovement);
		
		interpereter.info();
		return true;
	}

}
