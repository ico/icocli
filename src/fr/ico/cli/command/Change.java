package fr.ico.cli.command;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import fr.ico.cli.Command;
import fr.ico.cli.Interpereter;
import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.Optimization;
import fr.ico.lib.optimization.storage.Suggestion;
import fr.ico.lib.optimization.utils.Logger;

public class Change extends Command {
	
	public Change() {
		super("change");
		
		Option option_a = Option.builder("a").desc("The name of the feature to add").hasArg().longOpt("add").build();
		Option option_r = Option.builder("r").desc("The name of the feature to remove").hasArg().longOpt("remove").build();
		
		Option option_f = Option.builder("F").desc("List feature suggestions").longOpt("feature").build();
		Option option_i = Option.builder("I").desc("List interaction suggestions").longOpt("interaction").build();

		getOptions().addOption(option_f);
		getOptions().addOption(option_i);
		
		Option option_A = Option.builder("A").desc("Apply the suggestions").longOpt("apply").build();
		
		getOptions().addOption(option_a);
		getOptions().addOption(option_r);
		getOptions().addOption(option_A);
	}

	@Override
	public boolean command(CommandLine commandLine, Interpereter interpereter)
			throws ParseException, NoConfigurationException {
		// TODO Auto-generated method stub
		String toAdd = null;
		String toRemove = null;
		ConfigImprovement configImprovement = interpereter.getConfigImprovement();
		
		if (commandLine.hasOption("a")) {
			toAdd = commandLine.getOptionValue("a");
		}
		if (commandLine.hasOption("r")) {
			toRemove = commandLine.getOptionValue("r");
		}
		
		Optimization optimization = configImprovement.getFeatureOptimization();
		if (commandLine.hasOption("i")) {
			optimization = configImprovement.getInteractionOptimization();
		}
		
		Suggestion s = configImprovement.createSuggestion(optimization, toRemove, toAdd);
		
		Logger.log(s.toString());
		Logger.log("Configuration valid:" + configImprovement.isSuggestionValid(s));
		Logger.log("Configuration can be valid:" + configImprovement.canSuggestionBeValid(s));
		
		if (commandLine.hasOption("A")) {
			configImprovement.applySuggestion(s);
			Logger.log("Suggestion applied");
		}
		
		return false;
	}

}
