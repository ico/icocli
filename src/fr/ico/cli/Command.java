package fr.ico.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import fr.ico.cli.util.NoConfigurationException;
import fr.ico.lib.optimization.utils.Logger;

public abstract class Command {
	
	private Options options = new Options();
	private String commandName;
	
	public Command(String name) {
		this.commandName = name;
		
		Option option_h = Option.builder("h").required(false).desc("Display this help").longOpt("help").build();
		getOptions().addOption(option_h);
	}
	
	public abstract boolean command(CommandLine commandLine, Interpereter  interpereter) throws ParseException, NoConfigurationException;
	
	public boolean execute(String[] command, Interpereter  interpereter) {
		boolean result = false;
		try {
			CommandLineParser parser = new DefaultParser();
			CommandLine commandLine = parser.parse(getOptions(), command);
			
			if (commandLine.hasOption("h")) {
				help(commandName, getOptions());
			} else {
				result = command(commandLine, interpereter);
			}
			
		} catch (ParseException exception) {
			Logger.err("Parse error: ");
			Logger.err(exception.getMessage());
		} catch (NoConfigurationException e) {
			Logger.err("No configuration loaded: please load a configuration first");
		}
		return result;
	}

	public void help(String command, Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(command, options);
	}
	
	public Options getOptions() {
		return options;
	}

}
